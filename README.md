[![pipeline status](https://gitlab.com/wpdesk/wp-settings-field-boxes/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-settings-field-boxes/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-settings-field-boxes/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-settings-field-boxes/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-settings-field-boxes/v/stable)](https://packagist.org/packages/wpdesk/wp-settings-field-boxes) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-settings-field-boxes/downloads)](https://packagist.org/packages/wpdesk/wp-settings-field-boxes) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-settings-field-boxes/v/unstable)](https://packagist.org/packages/wpdesk/wp-settings-field-boxes) 
[![License](https://poser.pugx.org/wpdesk/wp-settings-field-boxes/license)](https://packagist.org/packages/wpdesk/wp-settings-field-boxes) 

# wp-settings-field-boxes

Before commit execute: `npm run prod`!
