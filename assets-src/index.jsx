import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ShippingBoxes from './components/shipping-boxes.jsx';

document.addEventListener('DOMContentLoaded', function () {
    let elements = document.getElementsByClassName("settings-field-boxes");
    let i;
    for (i = 0; i < elements.length; i++) {
        let element = elements[i];
        ReactDOM.render(
            <ShippingBoxes
                boxes={element.dataset.value}
                name={element.dataset.name}
                builtinboxes={element.dataset.builtinboxes}
                labels={element.dataset.labels}
                description={element.dataset.description}
            />,
            document.getElementById(element.id)
        );
    }
});
