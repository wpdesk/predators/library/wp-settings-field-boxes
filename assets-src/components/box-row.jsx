import React, { Component } from 'react';
import BoxInput from "./box-input.jsx";
import BoxType from "./box-type.jsx";

export default class BoxRow extends React.Component {
    constructor (props) {
        super(props);
        let box = props.box;
        let code = box.code;
        let builtIn = false;
        if ( props.builtInBoxes.filter(box => box.code == code).length ) {
            builtIn = true;
        }
        this.state = {
            box: props.box,
            builtInBoxes: props.builtInBoxes,
            builtIn: builtIn,
            code: code,
            inputNamePrefix: props.name,
            index: props.index,
            labels: props.labels,
        }

        this.handleTypeChange = this.handleTypeChange.bind(this);

    }

    handleTypeChange(type) {
console.log(this.state);
        let builtInBox = this.state.builtInBoxes.filter(box => box.code == type);
        this.state.box.code = type;
        if ( builtInBox.length ) {
            this.state.builtIn = true;
            let box = builtInBox[0];
            this.state.box.length = box.length;
            this.state.box.width = box.width;
            this.state.box.height = box.height;
            this.state.box.max_weight = box.max_weight;
        } else {
            this.state.builtIn = false;
        }
        this.setState(this.state);
    }

    render () {
        let field_name = this.state.inputNamePrefix + '[' + this.state.index + ']';

        let readonly = this.state.builtIn;

        let box = this.state.box;

        let labels = this.state.labels;

        return (
            <tr id={this.state.inputNamePrefix + '_' + this.state.index} className="shipping-box">
                <td>
                    <BoxType box={box} name={field_name + '[code]'} value={this.state.box.code} builtInBoxes={this.state.builtInBoxes} handleTypeChange={this.handleTypeChange}/>
                </td>
                <td>
                    <BoxInput box={box} field='length' name={field_name + '[length]'} type="dimension" value={this.state.box.length} readOnly={readonly} />
                </td>
                <td>
                    <BoxInput box={box} field='width' name={field_name + '[width]'} type="dimension" value={this.state.box.width} readOnly={readonly} />
                </td>
                <td>
                    <BoxInput box={box} field='height' name={field_name + '[height]'} type="dimension" value={this.state.box.height} readOnly={readonly} />
                </td>
                <td>
                    <BoxInput box={box} field='max_weight' name={field_name + '[max_weight]'} type="weight" value={this.state.box.max_weight} readOnly={readonly} />
                </td>
                <td>
                    <BoxInput box={box} field='box_weight' name={field_name + '[box_weight]'} type="weight" value={this.state.box.box_weight} />
                </td>
                <td>
                    <BoxInput box={box} field='padding' name={field_name + '[padding]'} type="dimension" value={this.state.box.padding} />
                </td>
                <td>
                    <button data-index={this.state.index} className="button shipping-boxes-delete" onClick={this.props.handleClickDelete}>{labels.button_delete}</button>
                </td>
            </tr>
        )
    }
}
