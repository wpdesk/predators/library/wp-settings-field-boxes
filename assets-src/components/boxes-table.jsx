import React, { Component } from 'react';
import BoxRow from "./box-row.jsx";

export default class BoxesTable extends React.Component {

    constructor (props) {
        super(props);
        let elements = 0;
        let boxes = props.boxes;
        boxes.forEach(function(element){
            element.id = elements;
            elements++;
        })
        this.state = {
            boxes: boxes,
            builtInBoxes: props.builtInBoxes,
            name: props.name,
            elements: elements,
            labels: props.labels,
        };

    }

    handleClickAdd(event) {
        event.preventDefault();
        let state = this.state;
        state.elements++;
        state.boxes.push({
            id: state.elements,
            code: 'custom',
            length: '',
            width: '',
            height: '',
            max_weight: '',
            box_weight: 0,
            padding: 0,
        });
        this.setState( state );
    }

    handleClickDelete(event) {
        event.preventDefault();
        this.state.boxes = this.state.boxes.filter(box => box.id != event.target.dataset.index);
        this.setState(this.state);
    }

    render () {
        let labels = this.state.labels;

        return (
                <table className="shipping-boxes">
                    <thead>
                    <tr>
                        <td className="cell-string">{labels.header_type}</td>
                        <td className="cell-number">{labels.header_length}</td>
                        <td className="cell-number">{labels.header_width}</td>
                        <td className="cell-number">{labels.header_height}</td>
                        <td className="cell-number">{labels.header_max_weight}</td>
                        <td className="cell-number">{labels.header_box_weight}</td>
                        <td className="cell-number">{labels.header_padding}</td>
                        <td className="cell-action"> </td>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.boxes.map((box) =>
                            <BoxRow
                                key={box.id}
                                index={box.id}
                                builtInBoxes={this.state.builtInBoxes}
                                box={box}
                                name={this.props.name}
                                handleClickDelete={this.handleClickDelete.bind(this)}
                                labels={labels}
                            />
                        )
                    }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="8">
                                <button className="button shipping-boxes-add" onClick={this.handleClickAdd.bind(this)}>{labels.button_add}</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
        )
    }
}
